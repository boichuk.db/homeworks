const navigation = document.getElementsByClassName('header__nav-menu')[0];
const hamburger = document.getElementsByClassName('header__hamburger')[0];
const iconHamburger = document.getElementsByClassName('header__hamburger-icon')[0];


$(document).ready(function() {
    $(hamburger).on('click', function() {
        $(navigation).toggleClass("header__nav-menu--active");
        $(iconHamburger).toggleClass("fa-times");
    });
});
