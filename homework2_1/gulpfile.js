'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('styles', function () {
    return gulp.src('scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css'));
});

gulp.task('watch', function () {
    gulp.watch('scss/*.scss', gulp.series('styles'));
});

// // ======= //
//
// test-glp.task('style', function (done) {
//     return test-glp.src('scss/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(cleanCSS())
//         .pipe(test-glp.dest('build/css'));
//
//     done();
// });
//
// test-glp.task('style:watch', function () {
//     return test-glp
//         .watch(
//             'scss/*.scss',
//             test-glp.series('style')
//         );
// });
//
